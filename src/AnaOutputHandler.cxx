#include "AnaOutputHandler.h"

#include <TFile.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TKey.h>
#include <math.h>
#include <stdlib.h>

#include "easylogging++.h"

TDirectory* AnaOutputHandler::Dir(std::string name) {
    std::lock_guard<std::mutex> lock{_lock}; // Need lock when writing in case multiple creating
    if (this->_oDirs.find(name) == this->_oDirs.end()) {
        _oFile->mkdir(name.c_str());
        this->_oDirs[name] = _oFile->GetDirectory(name.c_str());
    }
    return this->_oDirs[name];
}

TTree* AnaOutputHandler::newTree(std::string name) {
    std::lock_guard<std::mutex> lock{_lock}; // Need lock when writing in case multiple creating
    if (this->_oTrees.find(name) != this->_oTrees.end()) {
        LOG(FATAL) << "Tried to create a new tree while the output already have a tree with the same name! This will "
                      "cause memory leak and it's forbiden!";
    }
    this->_oTrees[name] = new TTree(name.c_str(), name.c_str());
    this->_oTrees[name]->SetAutoFlush(-3000000);
    this->_oTrees[name]->SetAutoSave(-3000000);
    this->_oTrees[name]->SetDirectory(_oFile);
    return this->_oTrees[name];
}

TTree* AnaOutputHandler::newTree(TTree* tree, std::string newname, Long64_t nentries, Option_t* option) {
    std::lock_guard<std::mutex> lock{_lock};
    std::string name = (newname == "") ? tree->GetName() : newname;
    if (this->_oTrees.find(name) != this->_oTrees.end()) {
        LOG(FATAL) << "Tried to copy a tree while the output already have a tree with the same name! This will cause "
                      "memory leak and it's forbiden!";
    }
    this->_oTrees[name] = tree->CloneTree(nentries, option);
    if (newname != "") {
        this->_oTrees[name]->SetName(newname.c_str());
    }
    this->_oTrees[name]->SetAutoFlush(-3000000);
    this->_oTrees[name]->SetAutoSave(-3000000);
    this->_oTrees[name]->SetDirectory(_oFile);
    return this->_oTrees[name];
}

void AnaOutputHandler::deleteTree(std::string name) {
    std::lock_guard<std::mutex> lock{_lock};
    SafeDelete(this->_oTrees[name]);
}

TTree* AnaOutputHandler::getTree(std::string name) {
    auto iter = this->_oTrees.find(name);
    if (iter != this->_oTrees.end()) {
        return iter->second;
    }
    LOG(ERROR) << "GetTree: No tree named " << name << ". return nullptr";
    return nullptr;
}

void AnaOutputHandler::Write() {
    // Write everything if make ntuple
    std::lock_guard<std::mutex> lock{_lock};
    _oFile->Write("", TObject::kOverwrite);
}

AnaOutputHandler::~AnaOutputHandler() {
    _oFile->Close();
    SafeDelete(_oFile);
}
