#include "Cutflow.h"

#include "easylogging++.h"

void Cutflow::checkCutName(std::string cutname) {
    for (unsigned int i = 0; i < CutQueue.size(); i++) {
        CHECK(cutname != CutQueue.at(i)->name) << "In case you regster a cut into a loop which may cause a disater in mermory,"
                                               << " You can't register two cut with same name"
                                               << " The wrong cut name is: " << cutname;
    }
}

// if disabled histogram fill
std::shared_ptr<CutIterator> Cutflow::registerCut(std::string cutname, std::function<bool()> cut) {
    checkCutName(cutname);
    std::shared_ptr<CutIterator> newCut(new CutIterator(cut, this->willFillHist));
    newCut->setDirectory(this->histFolder);
    newCut->name = cutname;
    CutQueue.emplace_back(newCut);
    return newCut;
}

// if enabled histogram fill
std::shared_ptr<CutIterator> Cutflow::registerCut(std::string cutname, std::function<bool()> cut, std::string histname, int nBin, double binStart,
                                                  double binEnd, std::function<double()> fillPosition, bool useOverflow) {
    checkCutName(cutname);
    std::shared_ptr<CutIterator> newCut(new CutIterator(cut, this->willFillHist));
    newCut->setDirectory(this->histFolder);
    newCut->set_Before_Hist(histname, nBin, binStart, binEnd, fillPosition, useOverflow);
    std::string histname_N_1 = histname + "_N_1";
    newCut->set_N_1_Hist(histname_N_1, nBin, binStart, binEnd, fillPosition, useOverflow);
    newCut->name = cutname;
    CutQueue.emplace_back(newCut);
    return newCut;
}

void Cutflow::startCut() {
    int falseTimes(0), falsePosition(0);
    double fillNumber = weight();

    for (unsigned int i = 0; i < CutQueue.size(); i++) {
        // Fill hists before this cut if doesn't fail previous cuts and we have defined hist for this cut
        if (willFillHist && CutQueue[i]->hist_beforeCut != nullptr && falseTimes == 0) {
            CutQueue[i]->hist_beforeCut->Fill(fillNumber);
        }
        // Do cut
        if (CutQueue[i]->cut()) {
            // Check if it fails previous cuts
            if (falseTimes == 0) {
                CutQueue[i]->sum += fillNumber;
                CutQueue[i]->err = sqrt(pow(CutQueue[i]->err, 2) + pow(fillNumber, 2));

                // if enable fill hist, we will fill cutflow histogram
                auto hists = CutQueue[i]->hists;
                if (willFillHist && hists.size() > 0) {
                    for (unsigned int i = 0; i < hists.size(); i++) {
                        hists[i]->Fill(fillNumber);
                    }
                }
            }
        } else if (!willFillHist) {
            // if willFillHist set to 'false' particularly, it means we won't fill histgram and we won't need
            // to do following thing. So we just return to save time
            return;
        } else {
            falseTimes++;
            falsePosition = i;
        }
    }

    // fill N-1 Histogram
    // If this event fails >=2 cut, there is no any historgram need to fill
    if (falseTimes < 2 && willFillHist) {
        // If this event fails 1 cut, we noly need to fill that historgram
        if (falseTimes == 1) {
            if (CutQueue[falsePosition]->hist_N_1 != nullptr) {
                CutQueue[falsePosition]->hist_N_1->Fill(fillNumber);
            }
        }
        // If all pass, will Fill all of the N-1 hists we defined previously. Besides
        else {
            for (unsigned int i = 0; i < CutQueue.size(); i++) {
                if (CutQueue[i]->hist_N_1 != nullptr) {
                    CutQueue[i]->hist_N_1->Fill(fillNumber);
                }
            }
        }
    }

    // Fill output tree if pass all of the cuts
    if (falseTimes == 0 && willFillTree) {
        std::lock_guard<std::mutex> lock{_lock};
        outTree->Fill();
    }
}

void Cutflow::PrintOut(bool CoutRatio, int OutputStyle) { PrintOut("cutflow", CoutRatio, OutputStyle); }

void Cutflow::PrintOut(std::string name, bool coutRatio, int OutputStyle) {
    if (OutputStyle == CSV) {
        PrintOutCsv(name, coutRatio);
    } else if (OutputStyle == LATEX) {
        PrintOutLatex(name, coutRatio);
    } else {
        LOG(WARNING) << "Unknown output type. Currrently only support CSV, LATEX. Choose CSV as default." << std::endl;
        PrintOutCsv(name, coutRatio);
    }
}

void Cutflow::PrintOutCsv(std::string name, bool coutRatio) {
    unsigned int i;
    std::string outname = name + ".csv";
    std::ofstream cutcout(outname, std::ios::app);
    std::ofstream yield("yield.csv", std::ios::app);

    // print out cutflow
    cutcout << "Cut name, " << CutQueue[0]->name << ", ";
    for (i = 1; i < CutQueue.size() - 1; i++) {
        CutQueue[i]->rate = (CutQueue[i]->sum) / (CutQueue[i - 1]->sum) * 100;
        cutcout << CutQueue[i]->name << ", ";
    }
    CutQueue[i]->rate = (CutQueue[i]->sum) / (CutQueue[i - 1]->sum) * 100;
    cutcout << CutQueue[i]->name << std::endl;

    std::string writeName = (Name != "") ? Name : "Event remain";

    cutcout << writeName << ", ";
    for (i = 0; i < CutQueue.size() - 1; i++) {
        Utils::roundCSV(cutcout, CutQueue[i]->sum, CutQueue[i]->err);
        cutcout << ",";
    }
    Utils::roundCSV(cutcout, CutQueue[i]->sum, CutQueue[i]->err);
    cutcout << std::endl;

    // if enable ratio print
    if (coutRatio) {
        cutcout << "Ratio, ";
        for (i = 0; i < CutQueue.size() - 1; i++) {
            cutcout << std::fixed << std::setprecision(2) << CutQueue[i]->rate << "%, ";
        }
        cutcout << std::fixed << std::setprecision(2) << CutQueue[i]->rate << "%" << std::endl;
    }

    // printout yield
    auto it = *CutQueue.rbegin();
    yield << writeName << ", ";
    Utils::roundCSV(yield, it->sum, it->err);
    yield << std::endl;
}

void Cutflow::PrintOutLatex(std::string name, bool coutRatio) {
    // Todo
    unsigned int i;
    std::string outname = name + ".tex";
    std::ofstream cutcout(outname, std::ios::app);
    std::ofstream yield("yield.tex", std::ios::app);

    // print out cutflow
    cutcout << "Cut name  &  " << CutQueue[0]->name << "  &  ";
    for (i = 1; i < CutQueue.size() - 1; i++) {
        CutQueue[i]->rate = (CutQueue[i]->sum) / (CutQueue[i - 1]->sum) * 100;
        cutcout << CutQueue[i]->name << "  &  ";
    }
    CutQueue[i]->rate = (CutQueue[i]->sum) / (CutQueue[i - 1]->sum) * 100;
    cutcout << CutQueue[i]->name << "  \\\\ " << std::endl;

    std::string writeName = (Name != "") ? Name : "Event remain";

    cutcout << writeName << "  &  ";
    for (i = 0; i < CutQueue.size() - 1; i++) {
        Utils::roundLatex(cutcout, CutQueue[i]->sum, CutQueue[i]->err);
        cutcout << "  &  ";
    }
    Utils::roundLatex(cutcout, CutQueue[i]->sum, CutQueue[i]->err);
    cutcout << "  \\\\" << std::endl;

    // if enable ratio print
    if (coutRatio) {
        cutcout << "Ratio  &  ";
        for (i = 0; i < CutQueue.size() - 1; i++) {
            cutcout << std::fixed << std::setprecision(2) << CutQueue[i]->rate << "%  &  ";
        }
        cutcout << std::fixed << std::setprecision(2) << CutQueue[i]->rate << "%  \\\\" << std::endl;
    }

    // printout yield
    auto it = *CutQueue.rbegin();
    yield << writeName << "  &  ";
    Utils::roundLatex(yield, it->sum, it->err);
    yield << "  \\\\" << std::endl;
}
