import json, argparse


parser = argparse.ArgumentParser()
parser.add_argument('-i', dest="input", action="store", help="comma split input json files", default=None)
parser.add_argument('-o', dest="output", action="store", help="the output merged json file name", default=None)
parser.add_argument('-p', '--usePlus', dest="useSum", nargs="+", help="merge json node with these key name by sum up", default=[])

args = parser.parse_args()
merge_key_list = args.useSum

def mergeList(list1, list2):
    for l in list2:
        if l not in list1:
            list1.append(l)


def mergeDict(dict1, dict2, usePlus = False):
    for key in dict2.keys():
        value = dict2[key]
        if not key in dict1:
            dict1[key] = value
        else:
            oldVar = dict1[key]
            if isinstance(oldVar, type(value)):
                if key in merge_key_list or usePlus:
                    if isinstance(value, dict): # dict2[key] is a dict
                        mergeDict(dict1[key], value, True)
                    elif isinstance(value, list): #dict2[key] is a list
                        mergeList(oldVar, value)
                    else:
                        dict1[key] = value + oldVar
                else:
                    if isinstance(value, dict): # dict2[key] is a dict
                        mergeDict(dict1[key], value)
                    else:
                        dict1[key] = value
            else:
                dict1[key] = value


def main():
    inputFiles = args.input
    outputFile = args.output
    jOut = {}
    if inputFiles is not None and outputFile is not None:
        jInputs = inputFiles.split(',')
        for jIn in jInputs:
            with open(jIn, 'r')as f:
                dict_json = json.load(f)
                mergeDict(jOut, dict_json)
        with open(outputFile, 'w')as fNew:
            json.dump(jOut, fNew, indent=4)


if __name__ == '__main__':
    main()
