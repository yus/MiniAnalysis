#include <TError.h>

#include <boost/program_options.hpp>
#include <iostream>

#include "AnaConfigReader.h"
#include "AnaRunner.h"
#include "MetaDB.h"
#include "ThreadPool.h"
#include "Utils.h"

int MainProcess(int argc, char* argv[] = NULL) {
    using json = nlohmann::json;
    namespace po = boost::program_options;

    std::string outputName = "";
    double exceptionWei = 1;
    unsigned int inputThreadNum = 1;

    po::options_description desc("A fairly simple and mini Analysis");
    desc.add_options()("help,h", "print this message and exit")("listAnalysises,l", "list all of the supported analysises and exit")(
        "verbose,v", "show debug and verbose log messages")("analysis,a", po::value<std::string>(), "choose the analysis you want to use")(
        "config-file,c", po::value<std::string>(),
        "The config file you will use. Note the program option will override the setting in the config file")(
        "output,o", po::value<std::string>(&outputName),
        "Output name - if not supplied, will use the name of the first inputFile with removing dir path")("input-files,i", po::value<std::string>(),
                                                                                                          "Comma-separated list of input files")(
        "weight,w", po::value<double>(&exceptionWei), "Optional: Don't use default sample weight and define your own fill weight")(
        "thread,t", po::value<unsigned int>(&inputThreadNum),
        "Optional: When running multiple trees. How many thread you want to use to speed up. Default: 4")("MakeNtuple,n", "Fill ntuple");

    po::positional_options_description p;
    p.add("input-files", -1);

    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
    po::notify(vm);

    if (vm.count("help") || ((vm.count("input-files") == 0) && (vm.count("listAnalysises") == 0))) {
        std::cout << desc << std::endl;
        return 0;
    }

    if (vm.count("listAnalysises")) {
        std::vector<std::string> list = getAnaList();
        std::cout << "Supported analysises:" << std::endl;
        for (const auto& analysis : list) {
            std::cout << analysis << std::endl;
        }
        return 0;
    }

    // Log verbose setting
    if (vm.count("verbose")) {
        el::Loggers::reconfigureAllLoggers(el::Level::Debug, el::ConfigurationType::Enabled, "true");
        el::Loggers::reconfigureAllLoggers(el::Level::Verbose, el::ConfigurationType::Enabled, "true");
        gErrorIgnoreLevel = kError;
    }

    // Set Config file
    CHECK(vm.count("config-file")) << "*****************Error! No config file! Program terminated**********************";
    std::string configName = vm["config-file"].as<std::string>();
    AnaConfigReader::Instance().loadConfig(configName);

    // Set inputFile
    CHECK(vm.count("input-files")) << "********************Error! No input file! Program terminated****************";
    std::string infiles = vm["input-files"].as<std::string>();
    LOG(INFO) << "Files to analyze: " << infiles;
    AnaConfigReader::Instance().setInputs(infiles);

    // Set analysis
    if (vm.count("analysis")) {
        std::string analysis = vm["analysis"].as<std::string>();
        AnaConfigReader::Instance().setAnalysis(analysis);
    }

    if (vm.count("weight")) {
        LOG(WARNING) << "You force analysis use a specified weight (usually it's for looking at raw events), value is: " << exceptionWei;
        AnaConfigReader::Instance().setWeight(exceptionWei);
    } else if (AnaConfigReader::Instance().getOverriddenWeight() != 0) {
        LOG(INFO) << "Use default weight calculated from sample";
    }

    // Override the thread num if there is -t
    if (vm.count("thread")) {
        AnaConfigReader::Instance().setThreadNum(inputThreadNum);
    }

    bool doNtuple = vm.count("ntuple");
    if (doNtuple) {
        AnaConfigReader::Instance().setDoNtuple();
    }

    // If not secific output file name, will use the first inputFile with removing dir path '/'
    if (!vm.count("output")) {
        std::string firstFileFull = AnaConfigReader::Instance().getInputs()[0];
        outputName =
            Utils::splitStrBy(firstFileFull, '/').back(); // split the full path of the input and use the filename which should be the last one
    }
    AnaConfigReader::Instance().setOutput(outputName);
    LOG(INFO) << "Set the output file name as " << outputName;

    /********* Now apply analysis settings! Start the analysis! *****************************/
    // Read the selected analysis and make a test to check
    std::string analysisName = AnaConfigReader::Instance().getAnalysis();
    auto testRunner = getNewAnaRunner(analysisName);
    if (testRunner == nullptr) {
        LOG(FATAL) << "*********No analysises named " << analysisName
                   << "! Please use -l to see which analysises are supported! Program terminated*********";
    } else {
        delete testRunner;
    }
    LOG(INFO) << "Select analysis: " << analysisName;

    // get the Metadata DB
    std::vector<std::filesystem::path> metaFiles = AnaConfigReader::Instance().metadata();
    if (metaFiles.size() != 0) {
        for (auto&& meta : metaFiles) {
            MetaDB::Instance().loadMetaDBFile(meta);
        }
    }
    // Use the tree structure of the first file
    std::vector<std::string> treeNameRegs = AnaConfigReader::Instance().getTrees();
    std::vector<std::string> chains;
    std::string firstFile = AnaConfigReader::Instance().getInputs()[0];
    for (std::string reg : treeNameRegs) {
        std::vector<std::string> filteredchains = Utils::getTreeList(firstFile, reg);
        for (std::string c : filteredchains) {
            if (std::find(chains.begin(), chains.end(), c) == chains.end()) {
                chains.emplace_back(c);
            }
        }
    }
    LOG(DEBUG) << "get tree list done";
    // If we want to exclude some tree, loop all of the exclude reg and remove them.
    // Usually it's recommended to use while loop to erase vector:
    // https://stackoverflow.com/questions/4645705/vector-erase-iterator
    std::vector<std::string> excludedtreeNameRegs = AnaConfigReader::Instance().getExcludedTrees();
    for (const auto& reg : excludedtreeNameRegs) {
        std::regex pattern(reg);
        auto iter = chains.begin();
        while (iter != chains.end()) {
            if (Utils::regexMatch(pattern, *iter).size() != 0) {
                iter = chains.erase(iter);
            } else {
                ++iter;
            }
        }
    }

    AnaRunner* runner = getNewAnaRunner(AnaConfigReader::Instance().getAnalysis());
    runner->runChains(chains);
    delete runner;

    return 0;
}
