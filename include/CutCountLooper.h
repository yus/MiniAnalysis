#pragma once

#include <algorithm>
#include <functional>
#include <string>
#include <vector>

#include "AnaConfigReader.h"
#include "Runnable.h"
#include "Timer.h"

class CutCountLooper : public Runnable {
public:
    template <class T> using vector2D = std::vector<std::vector<T>>;
    TChain* anaChain;

    CutCountLooper() { this->weight = nullptr; };

    CutCountLooper(std::string chainName, std::vector<std::string> storedVars) {
        setAnaChain(chainName);
        this->_varNames = storedVars;
        this->_varFuncs = std::vector<std::function<float()>>(storedVars.size(), nullptr);
        this->weight = nullptr;
    };

    ~CutCountLooper() { SafeDelete(this->anaChain); }

    void setAnaChain(std::string chainName) {
        this->anaChain = new TChain(chainName.c_str());
        for (const auto& input : AnaConfigReader::Instance().getInputs()) {
            anaChain->Add(input.c_str());
        }
    }

    virtual void setVariables() = 0;
    virtual void loop() = 0;

    void run() override {
        setVariables();
        // Check if everything is set or not
        if (this->weight == nullptr) {
            LOG(FATAL) << "FATAL: The event weight for CutCount is not set! Please use setWeight(weightFunc) to set it";
        }
        for (int i = 0; i < _varFuncs.size(); i++) {
            if (_varFuncs[i] == nullptr) {
                LOG(FATAL) << "FATAL: The variable " << _varNames[i] << " for CutCount is not set! Please use setVar(name, varFunc) to set it";
            }
        }
        // The loop func will be write in the 'DefineCutCount()' marco for sepecific tree
        loop();
    }

    void setWeight(std::function<float()> weight) { this->weight = weight; }
    void setVar(std::string name, std::function<float()> value) {
        auto iter = std::find(_varNames.begin(), _varNames.end(), name);
        if (iter == _varNames.end()) {
            LOG(FATAL) << "No var named " << name << " is registered into the CutCount! Please check!";
        }
        _varFuncs[iter - _varNames.begin()] = value;
    }

    vector2D<float>&& returnTables() { return std::move(this->eventTables); }

protected:
    vector2D<float> eventTables;
    std::function<float()> weight;
    std::vector<std::string> _varNames;
    std::vector<std::function<float()>> _varFuncs;
};