#pragma once
#include <atomic>
#include <future>
#include <memory>
#include <queue>
#include <stdexcept>
#include <vector>

#include "Runnable.h"

class ThreadPool {
public:
    ThreadPool(unsigned int size = 4) {
        this->_poolSize = size;
        this->_poolStatus = RUNNING;
        addWorker(size);
    }

    void addTask(std::unique_ptr<Runnable> task) {
        std::lock_guard<std::mutex> lock(_lock);
        if (isRunning(_poolStatus)) {
            this->workQueue.push(std::move(task));
            _task_cv.notify_all();
        }
    }

    void addWorker(unsigned int size) {
        for (unsigned int i = 0; i < size; i++) {
            if (isRunning(_poolStatus)) {
                _worker.emplace_back([this] {
                    while (isProcessing(_poolStatus)) {
                        std::unique_ptr<Runnable> task;
                        // Lock area
                        {
                            std::unique_lock<std::mutex> lock{_lock};
                            _task_cv.wait(lock, [this] { return !workQueue.empty() || isShuttingDown(_poolStatus); });
                            if (isShuttingDown(_poolStatus) && workQueue.empty()) return;
                            task = std::move(workQueue.front());
                            workQueue.pop();
                        }
                        _idlThrNum--;
                        task->run();
                        _idlThrNum++;
                    }
                });
                _idlThrNum++;
            }
        }
    }

    unsigned int poolSize() { return this->_poolSize; }

    bool isRunning(int c) { return c < SHUTDOWN; }

    bool isProcessing(int c) { return c < STOP; }

    bool isShuttingDown(int c) { return c > RUNNING; }

    void shutDownPool() {
        _poolStatus = SHUTDOWN;
        for (std::thread& thread : _worker) {
            if (thread.joinable()) thread.join();
        }
    }

    void stopPool() {
        _poolStatus = STOP;
        for (std::thread& thread : _worker) {
            if (thread.joinable()) thread.join();
        }
    }

    void terminatePool() {
        _poolStatus = TERMINATED;
        for (std::thread& thread : _worker) {
            thread.detach();
        }
    }

    virtual ~ThreadPool(){
        // The default method is terminate. One could do 'waitFinish' or 'shutDownPool' before the terminate to avoid thread detach
        // terminatePool();
    };

private:
    /*The runState provides the main lifecycle control, taking on values :
     * RUNNING : Accept new tasksand process queued tasks
     * SHUTDOWN : Don't accept new tasks, but process queued tasks
     * STOP : Don't accept new tasks, don't process queued tasks, and interrupt in - progress tasks
     * TIDYING : All tasks have terminated, workerCount is zero, the thread transitioning to state TIDYING will run the terminated() hook method
     * TERMINATED : terminated() has completed
     *
     * The numerical order among these values matters, to allow ordered comparisons.The runState monotonically increases over time,
     * but need not hit each state.The transitions are :
     *
     * RUNNING->SHUTDOWN:  On invocation of shutdown(), perhaps implicitly in finalize()
     * (RUNNING or SHUTDOWN)->STOP:  On invocation of shutdownNow()
     * SHUTDOWN->TIDYING:  When both queueand pool are empty
     * STOP->TIDYING:  When pool is empty
     * TIDYING->TERMINATED:  When the terminated() hook method has completed
     *
     * Threads waiting in awaitTermination() will return when the state reaches TERMINATED. */
    const static int RUNNING = 1 << 1;
    const static int SHUTDOWN = 1 << 2; // todo: Currently only running check supported. Should use mutex and _task_cv to support others
    const static int STOP = 1 << 3;
    const static int TIDYING = 1 << 4;
    const static int TERMINATED = 1 << 5;

    unsigned int _poolSize;
    int _poolStatus;
    std::atomic<int> keepAliveTime{10};
    std::atomic<int> _idlThrNum{0};

    std::queue<std::unique_ptr<Runnable>> workQueue;
    std::vector<std::thread> _worker;
    std::mutex _lock;                 // lock for pool state switching
    std::condition_variable _task_cv; // condition variable for pool state switching
};
