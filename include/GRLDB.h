#pragma once

#include <string>
#include <unordered_map>
#include <vector>

class GRLDB {
    using Range = std::pair<int, int>;

public:
    void loadGRLFile(std::string filename);
    bool checkInGRL(int runNumber, int lumiBolck);

private:
    bool inRange(int num, std::vector<Range> ranges);

    std::unordered_map<int, std::vector<Range> > lumiBlocks;
};
