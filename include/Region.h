#pragma once
#include <fstream>
#include <functional>
#include <vector>

#include "AnaOutputHandler.h"
#include "Hist.h"
#include "Utils.h"

// A region is defined by cuts. We can also register some histograms into it
class Region {
public:
    const static int COMBINE_AND = 0;
    const static int COMBINE_OR = 1;

    Region(std::function<bool()> cut, std::shared_ptr<AnaOutputHandler> oFile, std::string histDirName = "") {
        this->name = "";
        this->cut = cut;
        sum = 0;
        err = 0;
        this->oFile = oFile;
        this->histDirName = histDirName;
        histFolder = (histDirName == "") ? oFile->rootDir() : oFile->Dir(histDirName);
    };
    Region(std::string name, std::function<bool()> cut, std::shared_ptr<AnaOutputHandler> oFile, std::string histDirName = "") {
        this->name = name;
        this->cut = cut;
        sum = 0;
        err = 0;
        this->oFile = oFile;
        this->histDirName = histDirName;
        histFolder = (histDirName == "") ? oFile->rootDir() : oFile->Dir(histDirName);
    };
    ~Region() {
        for (auto&& hist : hists) {
            SafeDelete(hist);
        }
    }
    void setCut(std::function<bool()> cut) { this->cut = cut; }
    void setName(std::string name) { this->name = name; }
    std::string getName() { return this->name; }
    void setWeight(std::function<double()> weight) { this->weight = weight; }
    // Usually the sysWei is added for syst histograms for a region
    void addHist(std::string name, int Nbins, double LowValue, double UpValue, std::function<double()> value, bool useOverflow = Hist::NO_OVERFLOW,
                 std::function<double()> sysWei = NULL) {
        auto newHist = new Hist(name, Nbins, LowValue, UpValue, value, useOverflow);
        newHist->SetDirectory(this->histFolder);
        hists.emplace_back(newHist);
        if (sysWei == NULL) {
            histExtraWei.emplace_back([&] { return 1; });
        } else {
            histExtraWei.emplace_back(sysWei);
        }
    }
    void addHist(Hist* hist, std::function<double()> sysWei = NULL) {
        hist->SetDirectory(this->histFolder);
        hists.emplace_back(hist);
        if (sysWei == NULL) {
            histExtraWei.emplace_back([&] { return 1; });
        } else {
            histExtraWei.emplace_back(sysWei);
        }
    }
    void addHist(HistTemplate histtemplate, std::string name, std::function<double()> sysWei = NULL) {
        auto newHist = histtemplate.createHist(name);
        newHist->SetDirectory(this->histFolder);
        hists.emplace_back(newHist);
        if (sysWei == NULL) {
            histExtraWei.emplace_back([&] { return 1; });
        } else {
            histExtraWei.emplace_back(sysWei);
        }
    }
    void fill() {
        if (cut()) {
            sum += weight();
            err = sqrt(err * err + weight() * weight());
            int i = 0;
            for (auto&& hist : hists) {
                double finalWei = weight() * histExtraWei[i]();
                hist->Fill(finalWei);
                i++;
            }
        }
    }
    void printOut(std::string output = "", std::string marker = "") {
        std::string name;
        if (output == "") {
            name = "yield.csv";
        } else {
            name = output + ".csv";
        }
        std::ofstream yield(name, std::ios::app);
        yield << marker << ", ";
        Utils::roundCSV(yield, sum, err);
        yield << std::endl;
    }
    std::shared_ptr<Region> combine(int combineMethod, std::function<bool()> additionCut, std::string histDirName = "") {
        std::function<bool()> currentCut = cut;
        std::function<bool()> newCut;
        if (combineMethod = COMBINE_AND) {
            newCut = [=] { return (currentCut() && additionCut()); };
        } else if (combineMethod = COMBINE_OR) {
            newCut = [=] { return (currentCut() || additionCut()); };
        } else {
            LOG(ERROR) << "Region combination: Unknown combine method. Currently only combine method Region::COMBINE_AND and Region::COMBINE_OR are "
                          "supported. For now, won't do anything";
            newCut = currentCut;
        }
        std::shared_ptr<Region> newRegion(new Region(newCut, this->oFile, histDirName));
        newRegion->setWeight(this->weight);
        return newRegion;
    }

private:
    double sum, err;
    std::function<double()> weight;
    std::shared_ptr<AnaOutputHandler> oFile;
    std::function<bool()> cut;
    std::vector<Hist*> hists;
    std::vector<std::function<double()>> histExtraWei;
    std::string histDirName;
    std::string name;
    TDirectory* histFolder;
};
