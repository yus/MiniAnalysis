#include "AnaObjs.h"
#include "CutCountRunner.h"
#include "PhyUtils.h"
#include "XamppTree.h"
#include "easylogging++.h"

DefineCutCount(Wh_CutCount, XamppTree);

void Wh_CutCount::setCutSteps() {
    addStep("MT2", {0, 40, 50, 60, 70, 80, 90, 100, 110}, '>');
    addStep("MttLow", {60, 70, 80, 90, 100, 110, 120, 130, 140}, '>');
    addStep("MttHigh", {60, 70, 80, 90, 100, 110, 120, 130, 140}, '<');
    addStep("tau1Pt", {20, 30, 40, 50, 60, 70, 80}, '>');
    addStep("NTightTau", {-1, 0, 1}, '>');
    addStep("tau2Pt", {20, 30, 40, 50}, '>');
    addStep("mtLep", {0, 20, 40, 60, 80, 90, 100}, '>');
}

void Wh_CutCount::setResultFilters() {
    addYieldsFilter("W_Nominal", 0);
    addYieldsFilter("Z_Nominal", 0);
    addYieldsFilter("Boson_Nominal", 0);
    addYieldsFilter("Top_Nominal", 0);
    addYieldsFilter("Higgs_Nominal", 0);
    addRelErrorFilter("W_Nominal", 1);
    addRelErrorFilter("Z_Nominal", 1);
    addRelErrorFilter("Boson_Nominal", 0.3);
    addRelErrorFilter("Top_Nominal", 0.5);
    setZnFilter(1.4);
}

void Looper_Wh_CutCount::setVariables() {
    setVar("MT2", [&] { return tree->mT2; });
    setVar("MttLow", [&] { return tree->Mtt; });
    setVar("MttHigh", [&] { return tree->Mtt; });
    setVar("tau1Pt", [&] { return tree->tau1Pt; });
    setVar("NTightTau", [&] { return tree->NTightTau; });
    setVar("tau2Pt", [&] { return tree->tau2Pt; });
    setVar("mtLep", [&] { return tree->lepMt; });
    setWeight([&] { return tree->Weight_mc; });
}
